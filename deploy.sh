#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'Cara pakai : deploy.sh <namafile.jar>'
    exit 1
fi

echo $1 > /var/lib/bukutamu/deploy.txt

chmod +x /var/lib/bukutamu/deploy.sh