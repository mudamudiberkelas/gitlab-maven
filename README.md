SERVER_TESTING_HOSTNAME
SSH_PRIVATE_KEY


deploy-ke-server-testing:
  stage: deploy
  image: debian:latest
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && ssh-keyscan -H "$SERVER_TESTING_HOSTNAME" > ~/.ssh/known_hosts'
  script:
    - scp target/*.jar root@$SERVER_TESTING_HOSTNAME:/var/lib/bukutamu/$CI_PROJECT_NAME-$CI_COMMIT_TAG.jar
    - ssh root@$SERVER_TESTING_HOSTNAME /var/lib/bukutamu/deploy.sh $CI_PROJECT_NAME-$CI_COMMIT_TAG.jar

deploy-ke-server-production:
  stage: deploy
  only:
    - /-RELEASE$/
  image: debian:latest
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && ssh-keyscan -H "$SERVER_TESTING_HOSTNAME" > ~/.ssh/known_hosts'
  script:
    - scp target/*.jar root@$SERVER_TESTING_HOSTNAME:/var/lib/bukutamu-production/$CI_PROJECT_NAME-$CI_COMMIT_TAG.jar
    - ssh root@$SERVER_TESTING_HOSTNAME /var/lib/bukutamu-production/deploy.sh $CI_PROJECT_NAME-$CI_COMMIT_TAG.jar